#include <WinSock2.h>
#include <vector>
#include <string>
#include <iostream>
#include "User.h"
#include "Helper.h"
#include "Protocol.h"



User::User(std::string userName, SOCKET socket)
{
	_userName.assign(userName);
	_socket = socket;
	_currGame = NULL;
	_currRoom = NULL;
}
User::~User()
{
	if (_currGame)
	{
		delete _currGame;
	}
	if (_currRoom)
	{
		delete _currRoom;
	}
}

void User::send(std::string message)
{
	Helper::sendData(this->_socket, message);
}


void User::setGame(Game* gm)
{
	_currRoom = NULL;
	_currGame = gm;
}

void User::clearGame()
{
	_currGame = NULL;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionTime, int questionsNo)
{
	if (this->_currRoom != NULL)
	{
		this->send("1141"); //the user is already in another room so he cannot create a room
		return false;
	}
	else
	{
		Room* newRoom = new Room(roomId, this, roomName, maxUsers, questionTime, questionsNo);
		_currRoom = newRoom;
		this->send("1140"); //created the room
		return true;
	}
}

bool User::joinRoom(Room* newRoom)
{
	if (_currRoom) //if the user is in a room
	{
		return false;
	}
	_currRoom = newRoom;
	newRoom->joinRoom(this);
	return true;
}

void User::leaveRoom()
{
	if (this->_currRoom != NULL) //if the user is in the room - get him out
	{
		this->_currRoom->leaveRoom(this);
	}

	this->_currRoom = NULL;
}

int User::closeRoom()
{
	int idClosedRoom = 0;

	if (this->_currRoom == NULL)
	{
		return -1;
	}

	if (this->_currRoom->closeRoom(this) != -1) //if the function closeRoom succssed then we do this
	{
		idClosedRoom = this->_currRoom->getId();
		delete this->_currRoom;
		this->_currRoom = NULL;
		return idClosedRoom;
	}
	return 0;
}

bool User::leaveGame()
{
	if (this->_currGame == NULL)
	{
		return false;
	}
	else
	{
		this->_currGame->leaveGame(this);
		this->_currGame = NULL;
		return true;
	}
}

void User::clearRoom()
{
	this->_currRoom = NULL;
}

std::string User::getUserName()
{
	return this->_userName;
}

SOCKET User::getSocket()
{
	return this->_socket;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}