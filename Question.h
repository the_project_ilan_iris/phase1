#pragma once 
#include <string>

class Question
{
private:
	std::string _question;
	std::string* _answers;
	int _correctAnswerIndex;
	int _id;

public:
	Question(int, std::string, std::string, std::string, std::string, std::string);
	std::string getQuestion();
	std::string getAnswers();
	int getCorrectAnswerIndex();
	int getId;
};