#pragma once

#include <string>
#include <vector>
#include <winsock2.h>
#include <Windows.h>
#include "Room.h"
#include "Game.h"


class Game;
class Room;

class User
{
public:
	User(std::string userName, SOCKET socket);
	~User();
	void send(std::string message);
	std::string getUserName();
	SOCKET getSocket();
	Room* getRoom();
	Game* getGame();
	void setGame(Game* gm);
	void clearRoom();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionTime, int questionsNo);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearGame();

private:
	std::string _userName;
	Room* _currRoom;
	Game* _currGame;
	SOCKET _socket;
};