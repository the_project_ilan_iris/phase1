#include "Game.h"

Game::Game(const std::vector<User*>& userslist, int, DataBase &db) : _db(db)
{
}

Game::~Game()
{
}


void Game::sendFirstQuestion()
{
}

void Game::handleFinishGame()
{
}

bool Game::handleNextTurn()
{
	return false;
}

bool Game::handleAnswerFromUser(User *, int, int)
{
	return false;
}

bool Game::leaveGame(User *)
{
	return false;
}

int Game::getID()
{
	return 0;
}
