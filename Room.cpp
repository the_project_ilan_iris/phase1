#include <vector>
#include <string>
#include <WinSock2.h>
#include <iostream>
#include <algorithm>
#include <sstream> 
#include "Room.h"
#include "Helper.h"
#include "Protocol.h"


Room::Room(int id, User* admin, std::string name, int maxUsers, int questionTime, int questionsNo)
{
	_id = id;
	_admin = admin;
	_name.assign(name);
	_maxUsers = maxUsers;
	_questionTime = questionTime;
	_questionsNo = questionsNo;

	_users.push_back(admin);
}
Room::~Room()
{

}
//the 108 request
std::string Room::getUsersListMessage()
{
	std::string message =std::to_string(Protocol::SEND_USERS_IN_ROOM);
	message += Helper::getPaddedNumber(_users.size(), 1);
	for (User* user : _users)
	{
		message += Helper::getPaddedNumber(user->getUserName().length(), 2);
		message += user->getUserName();
	}

	return message;
}

void Room::sendMessage(User* user, std::string message)
{
	try
	{
		for (int i = 0; i < _users.size(); i++)
		{
			_users[i]->send(message);
		}
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << '\n';
	}
}

std::string Room::getUsersAsString(std::vector<User*> users, User * admin)
{
	return std::string();
}

void Room::sendMessage(std::string message)
{
	this->sendMessage(NULL, message);
}

bool Room::joinRoom(User* newUser)
{
	std::string message = std::to_string(Protocol::JOIN_ROOM_ANSWER);

	if (_users.size() >= _maxUsers) //if the room is full
	{
		newUser->send(message + "1"); //message that means that the user couldnt join the room because the room is full
		return false;
	}
	else
	{	 // sending the 110 message - succsess to join to the room
		message += "0";
		message += Helper::getPaddedNumber(_questionsNo,2);
		message += Helper::getPaddedNumber(_questionTime, 2);
		newUser->send(message);

		//adding the user to the users list 
		_users.push_back(newUser);

		//sending 108 messgae to all the users
		this->sendMessage(getUsersListMessage());
	}
	return true;
}

void Room::leaveRoom(User* leavingUser)
{
	_users.erase(remove(_users.begin(), _users.end(), leavingUser), _users.end()); //removing the leavingUser from the users

	leavingUser->send(std::to_string(Protocol::LEAVE_ROOM_ANSWER) + "0"); //sending to the leavingUser that he is succsessfully got out from the room
	this->sendMessage(getUsersListMessage());//sending 108 request to all the users
}

int Room::closeRoom(User* user)
{
	if (user != _admin)
	{
		return -1;
	}

	sendMessage(std::to_string(Protocol::CLOSE_ROOM_ANSWER));

 	for (User* currUser : _users) //running on all the users(except the admin) the closeRoom function
	{
		//_users[]
		if (currUser != _admin)
		{
			/*if (currUser->getRoom() == this)
			{
				currUser->send((Protocol::LEAVE_ROOM_ANSWER) + "0");
			}*/
			currUser->clearRoom();
		}
	}
	return this->_id;
}

std::string Room::getUsersAsString()
{
	std::stringstream ss;
	for (size_t i = 0; i < _users.size(); ++i)
	{
		if (i != 0)
			ss << ",";
		ss << _users[i];
	}
	std::string s = ss.str();

	return s;
}

std::vector<User*> Room::getUsers()
{
	return std::vector<User*>();
}

int Room::getQuestionsNo()
{
	return this->_questionsNo;
}

int Room::getId()
{
	return this->_id;
}

std::string Room::getName()
{
	return this->_name;
}