#include "DataBase.h"
#include "Validator.h"

DataBase::DataBase()
{
}

DataBase::~DataBase()
{
}

bool DataBase::isUserExists(std::string user)
{
	if (_usersInDatabase.find(user) == _usersInDatabase.end())
	{
		// not found
		return false;
	}
	else 
	{
		// found
		return true;
	}
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	if (!(Validator::isUsernameValid(username) && Validator::isPasswordValid(password)))
	{
		return false;
	}
	if (isUserExists(username))
	{
		return false;
	}
	try
	{
		_usersInDatabase.insert(std::make_pair(username, std::make_pair(email, password)));
	}
	catch (...)
	{
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	if (isUserExists(username))
	{
		if (_usersInDatabase[username].second == password)
		{
			return true;
		}
	}
	return false;
}

std::vector<std::string> DataBase::getBestScores()
{
	return std::vector<std::string>();
}

std::vector<std::string> DataBase::getPersonalStatus(std::string)
{
	return std::vector<std::string>();
}

int DataBase::insertNewGame()
{
	return 0;
}

bool DataBase::updateGamesStatus(int)
{
	return false;
}

bool DataBase::addAnswerToPlayer(int, std::string, int, std::string, bool, int)
{
	return false;
}

int DataBase::callbackCount(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackQuestions(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackBestScores(void *, int, char **, char **)
{
	return 0;
}

int DataBase::callbackPersonalStatus(void *, int, char **, char **)
{
	return 0;
}
