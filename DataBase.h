#pragma once

#include <vector>
#include <string>
#include <map>
#include "Question.h"

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(std::string user);
	bool addNewUser(std::string, std::string, std::string);
	bool isUserAndPassMatch(std::string, std::string);
	std::vector<Question*> initQuestions(int);
	std::vector<std::string> getBestScores();
	std::vector<std::string> getPersonalStatus(std::string);
	int insertNewGame();
	bool updateGamesStatus(int);
	bool addAnswerToPlayer(int, std::string, int, std::string, bool, int);

	int static callbackCount(void*, int, char**, char**);
	int static callbackQuestions(void*, int, char**, char**);
	int static callbackBestScores(void*, int, char**, char**);
	int static callbackPersonalStatus(void*, int, char**, char**);

private:
	std::map<std::string, std::pair<std::string, std::string>> _usersInDatabase;
};